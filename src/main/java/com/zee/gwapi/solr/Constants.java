package com.zee.gwapi.solr;

import java.util.Arrays;
import java.util.List;

public class Constants {
  public static final String FIELDS = "fl";
  public static final String FILTER_QUERY = "fq";
  public static final String QUERY = "q";
  public static final String WT = "wt";
  public static final String GROUP = "group";
  public static final String GROUP_QUERY = "group.query";
  public static final String GROUP_LIMIT = "group.limit";
  public static final String INDENT = "indent";
  public static final String START = "start";
  public static final String ROWS = "rows";
  public static final String VERSION = "version";
  public static final String SORT = "sort";
  public static final String SOLR_RESULT = "solrResp";

  public static final String SOLR_GENERIC_SEARCH = "SOLR_GENERIC_SEARCH";
  public static final String SOLR_INTERNAL_SEARCH = "SOLR_INTERNAL_SEARCH";

  public static final List<String> SINGLE_VALUE_FIELDS = Arrays.asList(FIELDS, FILTER_QUERY, QUERY, WT, GROUP, GROUP_LIMIT, INDENT, START, ROWS, VERSION, SORT);
  public static final List<String> MULTI_VALUE_FIELDS = Arrays.asList(GROUP_QUERY);
}
