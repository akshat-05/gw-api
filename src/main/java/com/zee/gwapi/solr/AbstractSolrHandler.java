package com.zee.gwapi.solr;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;

import java.util.HashMap;

public abstract class AbstractSolrHandler {

  public SolrClient client = null;
  public Vertx vertx = null;

  public AbstractSolrHandler(SolrClient client, Vertx vertx){
    this.client = client;
    this.vertx = vertx;
  }

  public void handle(Message<Object> msg){

    if(client==null)
      return;

    getResponse(getQuery((JsonObject) msg.body()));
  }

  public abstract HashMap<String, Object> getQuery(JsonObject request);

  public abstract void getResponse(HashMap<String, Object> request);

}
