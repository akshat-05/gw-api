package com.zee.gwapi.solr;

import com.englishtown.vertx.solr.SolrService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SolrVerticle extends AbstractVerticle {

  SolrService service = null;
  SolrClient sClient = null;

  final Logger log = LoggerFactory.getLogger(SolrVerticle.class);

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    configureSolr()
      .compose(this::configureEventBus)
      .onSuccess(startPromise::complete)
      .onFailure(startPromise::fail);
  }

  Future<Void> configureSolr(){
    log.info("SolrVerticle Config : "+config().toString());

    sClient = new HttpSolrClient.Builder(config().getString("url")).build();

    return Future.future(promise -> promise.complete());
  }

  Future<Void> configureEventBus(Void v){

    EventBus bus = vertx.eventBus();

    for(SolrHandlerFactory handler :SolrHandlerFactory.values()){
      bus.consumer(handler.getTopic(), handler.getHandler(sClient, vertx)::handle);
    }

    return Future.future(promise -> promise.complete());
  }
}
