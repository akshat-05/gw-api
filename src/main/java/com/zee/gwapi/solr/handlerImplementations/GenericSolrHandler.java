package com.zee.gwapi.solr.handlerImplementations;

import com.zee.gwapi.solr.AbstractSolrHandler;
import com.zee.gwapi.solr.Constants;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.noggit.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class GenericSolrHandler extends AbstractSolrHandler {

  final static Logger log = LoggerFactory.getLogger(GenericSolrHandler.class);

  public GenericSolrHandler(SolrClient client, Vertx vertx){
    super(client, vertx);
  }

  public HashMap<String, Object> getQuery(JsonObject request){

    SolrQuery query = new SolrQuery();
    log.info("Req : {}", request.toString());

    request.fieldNames().forEach(key -> {
      if(Constants.SINGLE_VALUE_FIELDS.contains(key))
        query.set(key, request.getString(key));
      else
        log.info("Invalid Query Fields : {}", key);
    });

    HashMap<String, Object> responseMap = new HashMap<>();
    responseMap.put("Query", query);

    return responseMap;
  }

  public void getResponse(HashMap<String, Object> request){
    vertx.executeBlocking ( promise -> {
      try {
        QueryResponse response = client.query((SolrQuery) request.get("Query"));
        promise.complete(response);
      }catch(Exception e){
        e.printStackTrace();
      }
    }, response -> {
      SolrDocumentList docList = ((QueryResponse)response.result()).getResults();
      log.info("Total Returned Doc: " + docList.size());
      String resp = JSONUtil.toJSON(docList);
      log.info(resp);
      vertx.sharedData().getLocalMap("SOLR_RESP").put("DEBUG_RESP", resp);
    });
  }

}
