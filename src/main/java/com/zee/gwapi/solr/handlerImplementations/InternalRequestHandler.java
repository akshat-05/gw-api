package com.zee.gwapi.solr.handlerImplementations;

import com.zee.gwapi.server.Constants;
import com.zee.gwapi.solr.AbstractSolrHandler;
import com.zee.gwapi.utils.Utilities;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.GroupCommand;
import org.apache.solr.client.solrj.response.GroupResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

public class InternalRequestHandler extends AbstractSolrHandler {

  final static Logger log = LoggerFactory.getLogger(InternalRequestHandler.class);

  Utilities utils;

  public InternalRequestHandler(SolrClient client, Vertx vertx){
    super(client, vertx);
    this.utils = new Utilities();
  }

  @Override
  public HashMap<String, Object> getQuery(JsonObject request) {
    SolrQuery query = new SolrQuery();
    JsonObject req = request.getJsonObject(Constants.SOLR_QUERY);

    req.fieldNames().forEach(key -> {
      if(com.zee.gwapi.solr.Constants.SINGLE_VALUE_FIELDS.contains(key)) {
        log.info("Key:{}, Value:{}",key,req.getString(key));
        query.set(key, req.getString(key));
      } else if(com.zee.gwapi.solr.Constants.MULTI_VALUE_FIELDS.contains(key)){
        try {
          List<String> lst = req.getJsonArray(key).getList();
          String[] arr = new String[lst.size()];
          arr = lst.toArray(arr);
          log.info("Array Key : {}", key);
          for (String element: arr) {
            log.info(element);
          }
          query.set(key, arr);
        } catch (Exception e) {
          e.printStackTrace();
        }
      } else
        log.info("Invalid Query Fields : {}", key);
    });

    HashMap<String, Object> responseMap = new HashMap<>();

    responseMap.put("ResponseKey", request.getString(Constants.RESP_QUEUE));
    responseMap.put("Query", query);
    responseMap.put("Tag", request.getString("TAG"));

    return responseMap;
  }

  @Override
  public void getResponse(HashMap<String, Object> request){
    vertx.executeBlocking ( promise -> {
      try {
        QueryResponse response = client.query((SolrQuery) request.get("Query"));
        promise.complete(response);

      }catch(Exception e){
        e.printStackTrace();
      }
    }, response -> {

      try {

        SharedData sharedData = vertx.sharedData();
        LocalMap<String, Object> localMap = sharedData.getLocalMap("SOLAR_RESP");

        GroupResponse gResp = ((QueryResponse) response.result()).getGroupResponse();
        SolrDocumentList docList = ((QueryResponse) response.result()).getResults();

        if (docList != null) {
          log.info("Total Returned Doc: " + docList.size());
          String payload = "";
          JsonArray jsonArray = new JsonArray();
          for(SolrDocument doc : docList) {
            JsonObject jsonObject = new JsonObject();
            for(String key: doc.keySet()){
              jsonObject.put(key, doc.get(key));
            }
            jsonArray.add(jsonObject);
          }

          JsonObject obj = new JsonObject();
          obj.put(com.zee.gwapi.solr.Constants.SOLR_RESULT, jsonArray);

          payload = obj.toString();
          payload = utils.normalizeSolrResp(payload);

          log.info("Sending response to response key - {}", (String) request.get("ResponseKey"));
          JsonObject result = new JsonObject(payload);
          HashMap<String, String> map = utils.getSubString(response.toString(), "numFound\\s*=\\s*(?<total>[0-9]+),", "total");
          if(map!= null && map.get("total") != null)
            result.put("total", Long.parseLong(map.get("total")));
          else
            result.put("total", 0);

          localMap.put((String) request.get("ResponseKey"), result);
        } else if(gResp!=null){
          List<GroupCommand> gCommandList = gResp.getValues();
          for(GroupCommand gc: gCommandList ){
            gc.getValues().forEach(gp -> {
              SolrDocumentList gpDocList = gp.getResult();
              log.info("gpDocList:{}",gpDocList.toString());
              JsonArray jArray = new JsonArray();
              gpDocList.forEach(solrDoc -> {
                log.info("gpDocList:{}",solrDoc.toString());
                JsonObject jObj = new JsonObject();
                solrDoc.keySet().forEach(key -> {
                  log.info(key);
                  jObj.put(key, solrDoc.get(key));
                });
                log.info("jObj:{}", jObj);
                jArray.add(jObj);
              });
              log.info("jArray:{}",jArray.toString());
              localMap.put((String) request.get("ResponseKey"), jArray.toString());
            });
          }
        } else {
          log.warn("Solr Resp Null");
        }

      } catch(Exception e){
        e.printStackTrace();
      } finally {
        vertx.eventBus().send((String) request.get("ResponseKey"), (String) request.get("Tag"));
      }
    });
  }

}
