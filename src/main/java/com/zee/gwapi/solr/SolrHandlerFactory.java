package com.zee.gwapi.solr;

import com.zee.gwapi.solr.handlerImplementations.GenericSolrHandler;
import com.zee.gwapi.solr.handlerImplementations.InternalRequestHandler;
import io.vertx.core.Vertx;
import org.apache.solr.client.solrj.SolrClient;

public enum SolrHandlerFactory {
  GENERIC_HANDLER(Constants.SOLR_GENERIC_SEARCH) {
    @Override
    public AbstractSolrHandler getHandler(SolrClient client, Vertx vertx) {
      return new GenericSolrHandler(client, vertx);
    }
  },
  INTERNAL_REQ_HANDLER(Constants.SOLR_INTERNAL_SEARCH) {
    @Override
    public AbstractSolrHandler getHandler(SolrClient client, Vertx vertx) {
      return new InternalRequestHandler(client, vertx);
    }
  };

  private String eventBusTopic;

  SolrHandlerFactory(String eventBusTopic){
    this.eventBusTopic = eventBusTopic;
  }

  public String getTopic(){
    return eventBusTopic;
  }

  public abstract AbstractSolrHandler getHandler(SolrClient client, Vertx vertx);

}
