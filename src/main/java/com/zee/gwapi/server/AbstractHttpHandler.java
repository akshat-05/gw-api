package com.zee.gwapi.server;

import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public abstract class AbstractHttpHandler {

  protected String api;
  protected Vertx vertx;
  protected HashMap<String, Object> props;

  private static final Logger log = LoggerFactory.getLogger(AbstractHttpHandler.class);

  public AbstractHttpHandler(String api, Vertx vertx, HashMap<String, Object> props){
    this.api = api;
    this.vertx = vertx;
    this.props = props;
  }

  public abstract void handle(RoutingContext context);

}
