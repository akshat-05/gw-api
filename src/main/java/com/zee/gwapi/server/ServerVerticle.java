package com.zee.gwapi.server;

import com.zee.gwapi.server.factories.HttpRequestHandlerFactory;
import com.zee.gwapi.server.factories.ServiceRouteFactory;
import com.zee.gwapi.server.handlerImplementations.CustomTimeoutHandler;
import com.zee.gwapi.utils.ConfigCache;
import com.zee.gwapi.utils.Constants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.TimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

public class ServerVerticle extends AbstractVerticle {

  final static Logger log = LoggerFactory.getLogger(ServerVerticle.class);

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    configureRouter()
      .compose(this::configureApiRoutes)
      .compose(this::configureServiceRoutes)
      .compose(this::configureServer)
      .onSuccess(startPromise::complete)
      .onFailure(startPromise::fail);
  }

  Future<Router> configureRouter(){
    Router router = Router.router(vertx);

    return Future.future(promise -> promise.complete(router));
  }

  Future<Router> configureApiRoutes(Router router){

    String apiPath = null;
    String apiMethod = null;
    ConfigCache cachedRules = ConfigCache.DEFAULT;

    HashMap<String, Object> props = new HashMap<>();
    props.put(Constants.RESP_TIMEOUT, config().getInteger(Constants.RESP_TIMEOUT));
    props.put(Constants.TIMEOUT_MSG, config().getString(Constants.TIMEOUT_MSG));
    log.info("TimeOut:{}",config().getInteger(Constants.RESP_TIMEOUT));

    for(String api : (List<String>) config().getJsonArray(Constants.REG_APIs).getList()){

      apiPath = cachedRules.getApiPath(api);
      apiMethod = cachedRules.getApiMethod(api);
      log.info("Registering Route : {}, for api: {}", apiPath, api);

      switch(apiMethod){
        case "GET":
          log.info("HttpMethod: GET");
          router.get(apiPath)
            .handler(new CustomTimeoutHandler(config().getInteger(Constants.RESP_TIMEOUT), config().getInteger(Constants.TIMEOUT_CODE)))
            .handler(HttpRequestHandlerFactory.valueOf(apiMethod).getHandler(api, vertx, props)::handle);
          break;
        case "POST":
          log.info("HttpMethod: POST");
          router.post(apiPath)
            .handler(BodyHandler.create())
            .handler(new CustomTimeoutHandler(config().getInteger(Constants.RESP_TIMEOUT), config().getInteger(Constants.TIMEOUT_CODE)))
            .handler(HttpRequestHandlerFactory.valueOf(apiMethod).getHandler(api, vertx, props)::handle);
          break;
        default:
          log.warn("Invalid HttpMethod : {}", apiMethod);
      }
    }

    return Future.future(promise->promise.complete(router));
  }

  Future<Router> configureServiceRoutes(Router router){

    for(ServiceRouteFactory routeFactory : ServiceRouteFactory.values()){

      switch(routeFactory.getHttpMethod()){
        case "GET":
          log.info("Registering-GET_ServiceRoute : {} at {}", routeFactory.name(), routeFactory.getPath());
          router
            .get(routeFactory.getPath())
            .handler(routeFactory.getHandler(vertx)::handle);
          break;
        case "POST":
          log.info("Registering-POST_ServiceRoute : {} at {}", routeFactory.name(), routeFactory.getPath());
          router
            .post(routeFactory.getPath())
            .handler(BodyHandler.create())
            .handler(routeFactory.getHandler(vertx)::handle);
          break;
        case "PATCH":
          log.info("Registering-PATCH_ServiceRoute : {} at {}", routeFactory.name(), routeFactory.getPath());
          router
            .patch(routeFactory.getPath())
            .handler(routeFactory.getHandler(vertx)::handle);
          break;
        default:
          log.warn("[ServerRoutes] Invalid HttpMethod : {}", routeFactory.getHttpMethod());
      }
    }

    return Future.future(promise -> promise.complete(router));
  }

  Future<Void> configureServer(Router router){

    int port = config().getInteger(Constants.SERVER_PORT);
    log.info("Server Port : " + port);
    HttpServer server = vertx.createHttpServer().requestHandler(router);

    return Future.<HttpServer>future(promise -> server.listen(port, promise)).mapEmpty();
  }

}
