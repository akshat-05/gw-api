package com.zee.gwapi.server;

import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractServiceRouteHandler {
  protected Vertx vertx;

  private static final Logger log = LoggerFactory.getLogger(AbstractServiceRouteHandler.class);

  public AbstractServiceRouteHandler(Vertx vertx){
    this.vertx = vertx;
  }

  public abstract void handle(RoutingContext context);

}
