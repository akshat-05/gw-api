package com.zee.gwapi.server.factories;

import com.zee.gwapi.server.AbstractHttpHandler;
import com.zee.gwapi.server.handlerImplementations.GetRequestHandler;
import com.zee.gwapi.server.handlerImplementations.PostRequestHandler;
import io.vertx.core.Vertx;

import java.util.HashMap;

public enum HttpRequestHandlerFactory {

  POST {
    public AbstractHttpHandler getHandler(String api, Vertx vertx, HashMap<String, Object> props) {
      return new PostRequestHandler(api, vertx, props);
    }
  },GET {
    public AbstractHttpHandler getHandler(String api, Vertx vertx, HashMap<String, Object> props) {
      return new GetRequestHandler(api, vertx, props);
    }
  };

  public abstract AbstractHttpHandler getHandler(String api, Vertx vertx, HashMap<String, Object> props);

}
