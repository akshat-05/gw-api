package com.zee.gwapi.server.factories;

import com.zee.gwapi.server.AbstractServiceRouteHandler;
import com.zee.gwapi.server.handlerImplementations.ApiRuleConfigUpdateHandler;
import com.zee.gwapi.server.handlerImplementations.SolrDebugHandler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;

public enum ServiceRouteFactory {
  SOLAR_DEBUG(HttpMethod.POST, "/solr/debug/"){
    @Override
    public AbstractServiceRouteHandler getHandler(Vertx v) {
      return new SolrDebugHandler(v);
    }
  },
  API_RULE_CONFIG_UPDATE(HttpMethod.PATCH, "/update/ruleconfig/:apiId"){
    @Override
    public AbstractServiceRouteHandler getHandler(Vertx v) {
      return new ApiRuleConfigUpdateHandler(v);
    }
  };

  private HttpMethod httpMethod;
  private String path;

  ServiceRouteFactory(HttpMethod httpMethod , String path){
    this.httpMethod = httpMethod;
    this.path = path;
  }

  abstract public AbstractServiceRouteHandler getHandler(Vertx v);

  public String getHttpMethod(){
    return httpMethod.toString();
  }

  public String getPath(){
    return path;
  }

}
