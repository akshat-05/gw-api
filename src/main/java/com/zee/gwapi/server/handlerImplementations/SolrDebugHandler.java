package com.zee.gwapi.server.handlerImplementations;

import com.zee.gwapi.server.AbstractServiceRouteHandler;
import com.zee.gwapi.solr.Constants;
import com.zee.gwapi.utils.Utilities;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

public class SolrDebugHandler extends AbstractServiceRouteHandler {

  final static Logger log = LoggerFactory.getLogger(SolrDebugHandler.class);
  Utilities utils = new Utilities();

  public SolrDebugHandler(Vertx vertx){
    super(vertx);
  }

  @Override
  public void handle(RoutingContext context) {
    String reqJson = context.getBody().toString();
    JsonObject json = new JsonObject(reqJson);

    log.info("Solr Handler: " + reqJson);

    vertx.eventBus().send(Constants.SOLR_GENERIC_SEARCH, json);

    Future<Void> future = vertx.executeBlocking(promise -> {
      boolean state = true;
      LocalDateTime startTime = LocalDateTime.now();
      LocalDateTime endTime = null;
      while(vertx.sharedData().getLocalMap("SOLR_RESP") == null || !vertx.sharedData().getLocalMap("SOLR_RESP").containsKey("DEBUG_RESP")){
        endTime = LocalDateTime.now();

        if(startTime.until(endTime, ChronoUnit.MILLIS) > 500){
          state = false;
          break;
        }
      }

      if(state)
        promise.complete();
      else
        promise.fail("No Response Received");
    });

    if(CompositeFuture.all(Arrays.asList(future)).succeeded()){
      String resp = (String) vertx.sharedData().getLocalMap("SOLR_RESP").get("DEBUG_RESP");
      resp = utils.normalizeSolrResp(resp);

      context.response()
        .putHeader("content-type", "text/plain")
        .setStatusCode(200)
        .setStatusMessage("SUCCESS")
        .end(resp);
    }else{
      context.response()
        .putHeader("content-type", "text/plain")
        .setStatusCode(504)
        .setStatusMessage("FAILED")
        .end();
    }
  }
}
