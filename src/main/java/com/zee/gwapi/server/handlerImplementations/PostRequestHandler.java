package com.zee.gwapi.server.handlerImplementations;

import com.zee.gwapi.server.AbstractHttpHandler;
import com.zee.gwapi.utils.ApiRuleManager;
import com.zee.gwapi.utils.OperationContext;
import com.zee.gwapi.utils.Utilities;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class PostRequestHandler extends AbstractHttpHandler {

  private static final Logger log = LoggerFactory.getLogger(PostRequestHandler.class);
  private Utilities utils = new Utilities();

  public PostRequestHandler(String api, Vertx vertx, HashMap<String, Object> props){
    super(api, vertx, props);
    log.info("Instantiating Post Handler for {} api", api);
  }

  @Override
  public synchronized void handle(RoutingContext context) {

    try {

      if (context.getBody() == null)
        context.response().write("Invalid Request: Request Body Empty");

      log.info("{} api triggered", api);
      JsonObject request = new JsonObject(context.getBody().toString());

      OperationContext opContext = new OperationContext(request, context, vertx, props);

      ApiRuleManager ruleManager = new ApiRuleManager(api, opContext);
      opContext.setRuleManager(ruleManager);

      ruleManager.execute();
    }catch (Exception e) {
      e.printStackTrace();
    }

  }

}
