package com.zee.gwapi.server.handlerImplementations;

import com.zee.gwapi.db.Constants;
import com.zee.gwapi.server.AbstractServiceRouteHandler;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;

public class ApiRuleConfigUpdateHandler extends AbstractServiceRouteHandler {

  public ApiRuleConfigUpdateHandler(Vertx v){
    super(v);
  }

  @Override
  public void handle(RoutingContext context) {

    vertx.eventBus().send(Constants.BUS_UPDATE_API_RULE_CONFIGS, context.pathParam("apiId"));

    context
      .response()
      .setStatusCode(200)
      .setStatusMessage("SUCCESS")
      .end("Triggered Request");
  }
}
