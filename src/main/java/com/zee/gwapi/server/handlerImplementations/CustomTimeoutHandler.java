package com.zee.gwapi.server.handlerImplementations;

import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.TimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomTimeoutHandler implements TimeoutHandler {

  private int timeout;
  private int errorCode;
  final static Logger log = LoggerFactory.getLogger(CustomTimeoutHandler.class);

  public CustomTimeoutHandler(int timeout, int errorCode){
    this.timeout = timeout;
    this.errorCode = errorCode;
  }

  @Override
  public void handle(RoutingContext ctx) {
    long tid = ctx.vertx().setTimer(timeout, t -> {
      ctx.fail(errorCode);
      log.warn("[NOTE] RequestTimedOut !!!");
    });

    ctx.addBodyEndHandler(v -> ctx.vertx().cancelTimer(tid));
    ctx.next();
  }
}
