package com.zee.gwapi.server.handlerImplementations;

import com.zee.gwapi.server.AbstractHttpHandler;
import com.zee.gwapi.utils.ApiRuleManager;
import com.zee.gwapi.utils.OperationContext;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class GetRequestHandler extends AbstractHttpHandler {

  public static final Logger log = LoggerFactory.getLogger(GetRequestHandler.class);

  public GetRequestHandler(String api, Vertx vertx, HashMap<String, Object> props) {
    super(api, vertx, props);
    log.info("Instantiating Get Handler for {} api", api);
  }

  @Override
  public synchronized void handle(RoutingContext context) {

    try {

      JsonObject rq = new JsonObject();
      if (context.request().params() == null) {
        log.info("RequestParams Null");
      } else {
        for (Map.Entry<String, String> entry : context.request().params().entries()) {
          log.info("Entry {} : {}", entry.getKey(), entry.getValue());
          rq.put(entry.getKey(), entry.getValue());
        }
      }
      OperationContext opContext = new OperationContext(rq, context, vertx, props);
      ApiRuleManager ruleManager = new ApiRuleManager(api, opContext);

      ruleManager.execute();
    }catch (Exception e){
      e.printStackTrace();
    }
  }

}
