package com.zee.gwapi.server;

public class Constants {

  public static final String PARAM_DEFAULTS = "paramDefaults";
  public static final String MANDATORY_FIELDS = "mandatoryFields";
  public static final String SOLR_QUERY_BUILDER = "solrQueryBuilder";
  public static final String RESPONSE_QUEUE = "respQueue";
  public static final String SOLR_QUERY = "solrQuery";
  public static final String RESP_QUEUE = "respQueue";

}
