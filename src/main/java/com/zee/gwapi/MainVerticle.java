package com.zee.gwapi;

import com.englishtown.vertx.solr.SolrService;
import com.zee.gwapi.db.DbVerticle;
import com.zee.gwapi.server.ServerVerticle;
import com.zee.gwapi.solr.SolrVerticle;
import com.zee.gwapi.utils.ConfigCache;
import com.zee.gwapi.utils.Constants;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainVerticle extends AbstractVerticle {

  final static Logger log = LoggerFactory.getLogger(MainVerticle.class);
  JsonObject loadedConfig = new JsonObject();
  SolrService service = null;

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    getConfig()
      .compose(this::loadConfig)
      .compose(this::testEnv)
      .compose(this::setSystemProperties)
      .compose(this::deployPersistenceVerticles)
      .compose(this::deployServerVerticle)
      .onFailure(throwable -> {
        log.error(throwable.toString());
        throwable.printStackTrace();
        startPromise.fail(throwable.toString());
      })
      .onSuccess(startPromise::complete);
  }

  Future<JsonObject> getConfig(){

    ConfigStoreOptions fileStore = new ConfigStoreOptions()
      .setType("file")
      .setConfig(new JsonObject().put("path", "config.json"));

    ConfigStoreOptions sysPropsStore = new ConfigStoreOptions().setType("sys");

    ConfigRetrieverOptions options = new ConfigRetrieverOptions()
      .addStore(fileStore).addStore(sysPropsStore);

    ConfigRetriever retriever = ConfigRetriever.create(vertx, options);

    return retriever.getConfig();

  }

  Future<Void> loadConfig(JsonObject config){

    log.info("Config Size:"+config.fieldNames().size());

    loadedConfig.mergeIn(config);

    return Future.future(promise-> promise.complete());
  }

  Future<Void> testEnv(Void v){

    try {
      ArrayList<String> aList = new ArrayList<>();
      aList.add("Abc-1");
      aList.add("Abc-2");
      aList.add("Abc-3");
      aList.add("Abc-4");

      JsonObject rq = new JsonObject();
      rq.put("country", "IN");
      Map<String, Object> context = new HashMap<>();
      context.put("aList", aList);
      context.put("rq", rq);

    /*String[] lst = new String[10];
    for(String s: lst){
      s.trim().endsWith();
      s.replaceAll()
    }*/

      String expr2 = "String business_type_str=\"\";String busness_empty=\"\";\nforeach (val : aList) {\n business_type_str = business_type_str + \"\\\"\" + rq.getString(\"country\") + \":1:\" + val + \"\\\" OR  \";\n if (busness_empty.isEmpty()) {\nbusness_empty = \"\\\"\" + val + \"\\\"\";\n} else {\nbusness_empty = busness_empty + \" OR \" + \"\\\"\" + val + \"\\\"\";\n}\n}\n   System.out.println(\"business_type_str :\" + business_type_str);\nString rights = \"(\" + business_type_str + \")  OR (*:* -rights:[* TO *] AND business_type: (\" + busness_empty + \"))\";\nreturn rights;";

      String expr = "String str = \"My String\";\n" +
        "foreach (name : aList) {\n" +
        "   System.out.println(\"Person :\" + name);\n" +
        "   str = str.concat(name)" +
        "}\n" +
        "    \n" +
        "System.out.println(\"Total people: \" + str);" +
        "return str; ";

      final  String regex = "\\W\\w+\\s*(\\W*)$";
      String string = "#%$dd  ##";
      log.info("Original string : {}", string);
      final Pattern pattern = Pattern.compile(regex);
      final Matcher matcher = pattern.matcher(string);

      if (matcher.find()) {
        log.info("Full match: " + matcher.group(0));

        for (int i = 1; i <= matcher.groupCount(); i++) {
          log.info("Group " + i + ": " + matcher.group(i));
        }
      }

      String string1 = string.replaceFirst(regex, "X");
      String string2 = string.replaceFirst(regex, "X");
      log.info("string1 : {}", string1);
      log.info("string2 : {}", string2);
      String resp = MVEL.evalToString(expr2, context);
      log.info("Response : {}", resp);
    }catch(Exception e){
      e.printStackTrace();
    }


    return Future.future(promise -> promise.complete());
  }

  Future<Void> setSystemProperties(Void v){

    JsonArray array = loadedConfig.getJsonArray("systemProp");

    array.forEach( prop -> {
      if( prop instanceof JsonObject){
        JsonObject obj = (JsonObject) prop;
        log.info("Setting Property: " + obj.toString());
        System.setProperty(obj.getString("propName"), obj.getString("propValue"));
      }else{
        log.info("Property Obj Class Name : "+prop.getClass().getName());
      }
    });

    return Future.future(promise -> promise.complete());
  }

  Future<Void> deployPersistenceVerticles(Void v){

    ConfigCache.DEFAULT.setRegApis(loadedConfig.getJsonArray(Constants.REG_APIs).getList());

    Verticle dbV = new DbVerticle();

    Future<String> dbVerticle = Future.future( promise -> vertx.deployVerticle(new DbVerticle(),
      new DeploymentOptions()
      .setConfig(loadedConfig
                    .getJsonObject(Constants.DB_CONFIG)
                    .put(Constants.REG_APIs, loadedConfig.getJsonArray(Constants.REG_APIs))
      ), promise));

    Future<String> solrVerticle = Future.future(
      promise -> vertx.deployVerticle(new SolrVerticle(),
        new DeploymentOptions()
          .setConfig(loadedConfig.getJsonObject(Constants.SOLR_CONFIG)), promise) );

    return CompositeFuture.all(Arrays.asList(solrVerticle, dbVerticle))
      .onComplete(result ->{
        if(result.succeeded())
          log.info("All Persistence Verticles Deployed !!!");
        else
          log.info("Atleast One of the Persistence Verticles Failed !!!");
      }).mapEmpty();
  }

  Future<Void> deployServerVerticle(Void v){

    EventBus bus = vertx.eventBus();
    bus.consumer("TRIGGER_SERVER_DEPLOYMENT", msg -> {
      log.info(msg.body().toString());
      if(ConfigCache.DEFAULT.isInitialized()){
        log.info("Deploying ServerVerticle");

        vertx.deployVerticle(new ServerVerticle(),
          new DeploymentOptions().setConfig(loadedConfig
            .getJsonObject(Constants.SERVER_CONFIG)
            .put(Constants.REG_APIs, loadedConfig.getJsonArray(Constants.REG_APIs))
          ));
      }
    });

    return Future.future(promise -> promise.complete());
  }


}
