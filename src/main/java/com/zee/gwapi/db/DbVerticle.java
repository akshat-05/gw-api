package com.zee.gwapi.db;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

public class DbVerticle extends AbstractVerticle {

  final static Logger log = LoggerFactory.getLogger(DbVerticle.class);
  PgConnection pgConnection = null;

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    getConnection()
      .compose(this::initConnection)
      .compose(this::initCache)
      .compose(this::initBusHandler)
      .onSuccess(startPromise::complete)
      .onFailure(startPromise::fail);
  }

  Future<PgConnection> getConnection(){

    log.info(config().getString(Constants.URI));
    log.info(config().getJsonArray(com.zee.gwapi.utils.Constants.REG_APIs).toString());

    return PgConnection.connect(vertx, config().getString(Constants.URI));
  }

  Future<Void> initConnection(PgConnection con){
    pgConnection = con;
    return Future.future(promise -> promise.complete()).mapEmpty();
  }

  Future<Void> initCache(Void v){

    JsonObject cachedQueries = config().getJsonObject(Constants.CACHE_QUERIES);
    List<String> apiList = config().getJsonArray(com.zee.gwapi.utils.Constants.REG_APIs).getList();

    for(DbCacheQueryExecutor executor : DbCacheQueryExecutor.values()){
      executor.initCache(apiList, cachedQueries, pgConnection, config().getString(Constants.VERSION), vertx.eventBus());
    }

    return Future.future(promise -> promise.complete()).mapEmpty();
  }

  Future<Void> initBusHandler(Void v){

    try {
      HashMap<String, Object> params = new HashMap<>();
      params.put(Constants.VERSION, config().getString(Constants.VERSION));

      for(RegisteredDao dao : RegisteredDao.values()){
        dao
          .getHandler(vertx, pgConnection, config().getJsonObject(Constants.QUERIES).getString(dao.getQueryId()), params)
          .registerRequestEvent();
      }
    }catch (Exception e){
      e.printStackTrace();
    }

    return Future.future(promise -> promise.complete()).mapEmpty();
  }

}
