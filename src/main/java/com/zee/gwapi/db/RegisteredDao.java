package com.zee.gwapi.db;

import com.zee.gwapi.db.queryHandlers.ApiRuleConfigDao;
import io.vertx.core.Vertx;
import io.vertx.pgclient.PgConnection;

import java.util.HashMap;

public enum RegisteredDao {

  API_RULE_CONFIG("apiRuleConfigs"){
    @Override
    public QueryHandlers getHandler(Vertx v, PgConnection con, String query, HashMap<String, Object> params) {
      return new ApiRuleConfigDao(v, con, query, params);
    }
  };

  private String queryId;

  RegisteredDao(String queryId){
    this.queryId = queryId;
  }

  public String getQueryId(){
    return queryId;
  }

  public abstract QueryHandlers getHandler(Vertx v, PgConnection con, String query, HashMap<String, Object> params);
}
