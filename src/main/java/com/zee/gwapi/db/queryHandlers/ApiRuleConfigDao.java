package com.zee.gwapi.db.queryHandlers;

import com.zee.gwapi.db.Constants;
import com.zee.gwapi.db.QueryHandlers;
import com.zee.gwapi.utils.ConfigCache;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgConnection;
import io.vertx.sqlclient.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class ApiRuleConfigDao extends QueryHandlers {

  private static Logger log = LoggerFactory.getLogger(ApiRuleConfigDao.class);

  public ApiRuleConfigDao(Vertx v, PgConnection con, String query, HashMap<String, Object> params){
    super(v, con, query, params);
  }

  public void registerRequestEvent() {

    v.eventBus().consumer(Constants.BUS_UPDATE_API_RULE_CONFIGS, msg -> {
      con.preparedQuery(query)
        .execute(Tuple.of(msg.body(), params.get(Constants.VERSION)))
        .onSuccess( res -> {

          log.info("Fetched Rules for ApiId:{}, TotalRules : {}", msg.body().toString(), res.rowCount());

          HashMap<String, JsonObject> apiRules = new HashMap<>();
          res.forEach(row -> {
            String ruleKey = row.toJson().getString("group").concat("-").concat(row.toJson().getString("rule"));
            log.debug("Added rule key:{}", ruleKey);
            apiRules.put(ruleKey, row.toJson());
          });
          ConfigCache.DEFAULT.updateApiRuleCache(msg.body().toString(), apiRules);
      });
    });

  }
}
