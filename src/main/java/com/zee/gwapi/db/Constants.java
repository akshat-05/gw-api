package com.zee.gwapi.db;

public class Constants {
  public static final String URI = "uri";
  public static final String QUERIES = "queries";
  public static final String CACHE_QUERIES = "cache-queries";
  public static final String CACHE_API_PATH = "apiPath";
  public static final String CACHE_API_RULE_CONFIGS = "apiRuleConfigs";
  public static final String VERSION = "version";
  public static final String API_PATH = "api_path";

  public static final String BUS_UPDATE_API_RULE_CONFIGS = "UPDATE_API_RULE_CONFIGS";
}
