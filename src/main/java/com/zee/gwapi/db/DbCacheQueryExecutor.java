package com.zee.gwapi.db;

import com.zee.gwapi.utils.ConfigCache;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgConnection;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

public enum DbCacheQueryExecutor {

  API_PATH{
    @Override
    void initCache(List<String> apis, JsonObject cachedQueries, PgConnection pgConnection, String version, EventBus bus) {
      try {

        apis.forEach(api -> {
          pgConnection
            .preparedQuery(cachedQueries.getString(Constants.CACHE_API_PATH))
            .execute(Tuple.of(api, version), resp -> {
              if(resp.succeeded()) {
                HashMap<String, JsonObject> apiPathMap = new HashMap<>();
                RowSet<Row> rows = resp.result();
                if (rows == null)
                  log.info("API_PATH : Zero Rows Returned");
                else {
                  rows.forEach(rw -> {
                    log.info(rw.toJson().toString());
                    apiPathMap.put(api, rw.toJson());
                  });
                  ConfigCache.DEFAULT.setApiPathCache(apiPathMap);
                }
              }else{
                log.error("API_PATH : Cache loading Failed");
              }
              bus.send("TRIGGER_SERVER_DEPLOYMENT","API_PATH:".concat(api));
            });
        });

      }catch (Exception e){
        e.printStackTrace();
      }
    }
  },
  API_RULE_CONFIGS{
    @Override
    void initCache(List<String> apis, JsonObject cachedQueries, PgConnection pgConnection, String version, EventBus bus) {
      try{

        log.info("Version:{}", version);
        apis.forEach(api ->{
          HashMap<String, JsonObject> apiRules = new HashMap<>();
          pgConnection
            .preparedQuery(cachedQueries.getString(Constants.CACHE_API_RULE_CONFIGS))
            .execute(Tuple.of(api, version), resp -> {
              HashMap<String, HashMap<String, JsonObject>> apiRuleConfigs = new HashMap<>();
              if(resp.succeeded()) {
                RowSet<Row> rows = resp.result();
                if (rows == null)
                  log.info("API_RULE_CONFIGS : Zero Rows Returned");
                else {
                  log.info("API:{}, #Rules:{}", api, rows.size());
                  rows.forEach(rw -> {
                    String ruleKey = rw.toJson().getString("group").concat("-").concat(rw.toJson().getString("rule"));
                    log.debug("Rule key:{}, value:{}", ruleKey, rw.toJson().toString());
                    apiRules.put(ruleKey, rw.toJson());
                  });
                  apiRuleConfigs.put(api, apiRules);
                  ConfigCache.DEFAULT.setApiRuleCache(apiRuleConfigs);
                }
              }else{
                log.error("API_RULE_CONFIGS : Cache loading Failed");
              }
              bus.send("TRIGGER_SERVER_DEPLOYMENT","API_RULE_CONFIGS:".concat(api));
            });
        });

      }catch (Exception e){
        e.printStackTrace();
      }
    }
  };

  final static Logger log = LoggerFactory.getLogger(DbCacheQueryExecutor.class);
  abstract void initCache(List<String> apis, JsonObject cachedQueries, PgConnection pgConnection, String version, EventBus bus);

}
