package com.zee.gwapi.db;

import io.vertx.core.Vertx;
import io.vertx.pgclient.PgConnection;

import java.util.HashMap;

public abstract class QueryHandlers {

  protected Vertx v;
  protected PgConnection con;
  protected String query;
  protected HashMap<String, Object> params;

  public QueryHandlers(Vertx v, PgConnection con, String query, HashMap<String, Object> params){
    this.v = v;
    this.con = con;
    this.query = query;
    this.params = params;
  }

  public abstract void registerRequestEvent();
}
