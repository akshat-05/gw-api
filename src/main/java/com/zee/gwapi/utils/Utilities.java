package com.zee.gwapi.utils;

import org.apache.solr.common.util.Hash;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

  public String getValue(String value, String default_val){
    return (value==null || value.trim().isEmpty()) ? default_val : value;
  }

  synchronized public String normalizeSolrResp(String payload){
    payload = replaceRegex(payload, "\\\"\\{", "{");
    payload = replaceRegex(payload, "\\}\\\"", "}");
    payload = replaceRegex(payload, "\\\\\\\\n", "");
    payload = replaceRegex(payload, "\\{\\s*\\\\\\\"", "{\"");
    payload = replaceRegex(payload, "\\\\\\\"\\s*\\}", "\"}");
    payload = replaceRegex(payload, ":\\s*\\\\\\\"", ":\"");
    payload = replaceRegex(payload, "\\\\\\\"\\s*:", "\":");
    payload = replaceRegex(payload, "\\[\\s*\\\\\\\"", "[\"");
    payload = replaceRegex(payload, "\\\\\\\"\\s*]", "\"]");
    payload = replaceRegex(payload, "\\\\\\\"\\s*,\\s*\\\\\\\"", "\",\"");
    payload = replaceRegex(payload, "]\\s*,\\s*\\\\\\\"", "],\"");
    payload = replaceRegex(payload, "\\}\\s*,\\s*\\\\\\\"", "},\"");
    payload = replaceRegex(payload, "false*\\s*,\\s*\\\\\\\"", "false,\"");
    payload = replaceRegex(payload, "true*\\s*,\\s*\\\\\\\"", "true,\"");
    payload = replaceRegex(payload, "\\\\\\\\u", "\\\\u");
    return payload;
  }

  synchronized public String replaceRegex(String input, String regex, String replace) {

    final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
    final Matcher matcher = pattern.matcher(input);

    // The substituted value will be contained in the result variable
    final String result = matcher.replaceAll(replace);

    return result;
  }

  public HashMap<String, String> getSubString(String input, String regex, String... groups){
    final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
    final Matcher matcher = pattern.matcher(input);
    HashMap<String, String> map = new HashMap<>();

    if (matcher.find()) {
      System.out.println("Full match: " + matcher.toString());

      for(String g: groups){
        map.put(g, matcher.group(g));
      }
    }

    return map;
  }

}
