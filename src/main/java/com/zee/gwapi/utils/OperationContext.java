package com.zee.gwapi.utils;

import com.zee.gwapi.solr.Constants;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import org.jose4j.json.internal.json_simple.parser.ParseException;
import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class OperationContext {

  final static Logger log = LoggerFactory.getLogger(OperationContext.class);

  public JsonObject rq;
  Utilities util;
  public int intMaxValue = Integer.MAX_VALUE;
  public int intMinValue = Integer.MIN_VALUE;
  public RoutingContext context;
  public Vertx vertx;
  public MultiMap headers;
  private String instanceId;
  private HashMap<String, Object> props;
  private CommonHelper cHelper;
  private ApiRuleManager ruleManager;
  private List<MessageConsumer> registeredHandlers;

  public OperationContext(JsonObject rq, RoutingContext context, Vertx vertx, HashMap<String, Object> props) {
    this.rq = rq;
    this.context = context;
    this.vertx = vertx;
    instanceId = UUID.randomUUID().toString();
    this.props = props;
    util = new Utilities();
    headers = context.request().headers();
    cHelper = new CommonHelper();

    registerBusHandler();
  }

  public void clearContext(){
    if( registeredHandlers != null ){
      registeredHandlers.forEach( consumer -> {
        if(consumer!= null && consumer.isRegistered()) {
          log.info("Deregistering EventBusConsumer : {}", consumer.address());
          consumer.unregister();
        }
      });

      registeredHandlers = null;
    }
    cHelper = null;
    headers = null;
    context = null;
    rq = null;
    vertx = null;
    props = null;
    instanceId = null;
    ruleManager = null;
  }

  public boolean isContextFail(){
    if(context == null)
      return false;

    return context.failed();
  }

  public void setRuleManager(ApiRuleManager ruleManager){
    this.ruleManager = ruleManager;
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();

    if( registeredHandlers != null ){
      registeredHandlers.forEach( consumer -> {
        if(consumer!= null && consumer.isRegistered()) {
          log.info("Deregistering EventBusConsumer : {}", consumer.address());
          consumer.unregister();
        }
      });
    }

  }

  private void registerBusHandler(){
    registeredHandlers = new ArrayList<>();

    registeredHandlers.add(vertx.eventBus()
      .consumer(instanceId.concat("-SOLR_RESP_1"), msg -> {

        SharedData sd = vertx.sharedData();
        if (sd.getLocalMap("SOLAR_RESP").containsKey(instanceId.concat("-SOLR_RESP_1"))) {
          log.info("[SOLR_RESP_1] SolrResp - Received");
          rq.put((String) msg.body(), (JsonObject)sd.getLocalMap("SOLAR_RESP").remove(instanceId.concat("-SOLR_RESP_1")));
        } else {
          log.info("[SOLR_RESP_1] SolrResp - Not Received" );
        }

        log.info("Resuming Rule Execution...");
        this.ruleManager.execute();
      }));

    registeredHandlers.add(vertx.eventBus()
      .consumer(instanceId.concat("-SOLR_RESP_2"), msg -> {

        SharedData sd = vertx.sharedData();
        if (sd.getLocalMap("SOLAR_RESP").containsKey(instanceId.concat("-SOLR_RESP_2"))) {
          log.info("[SOLR_RESP_2] SolrResp - Received");
          rq.put((String) msg.body(), (JsonObject)sd.getLocalMap("SOLAR_RESP").remove(instanceId.concat("-SOLR_RESP_2")));
        } else {
          log.info("[SOLR_RESP_2] SolrResp - Not Received" );
        }

        log.info("Resuming Rule Execution...");
        this.ruleManager.execute();
      }));
  }

  public Future<Void> querySolr(String tag, String q, String fl, String groupLimit, String start,
                           String rows, String wt, List<String> groupQuery, String group, String version, String groupField,
                                String sort, String indent) {
    JsonObject qObj = new JsonObject();
    queryParamSetter(qObj, "q", q);
    queryParamSetter(qObj, "fl", fl);
    queryParamSetter(qObj, "group.limit", groupLimit);
    queryParamSetter(qObj, "start", start);
    queryParamSetter(qObj, "rows", rows);
    queryParamSetter(qObj, "wt", wt);
    queryParamSetter(qObj, "group", group);
    queryParamSetter(qObj, "group.query", groupQuery);

    queryParamSetter(qObj, "version", version);
    queryParamSetter(qObj, "group.field", groupField);
    queryParamSetter(qObj, "sort", sort);
    queryParamSetter(qObj, "indent", indent);

    JsonObject request = new JsonObject();
    request.put(com.zee.gwapi.server.Constants.SOLR_QUERY, qObj);
    request.put(com.zee.gwapi.server.Constants.RESP_QUEUE, instanceId.concat("-SOLR_RESP_1"));
    request.put("TAG", tag);

    this.ruleManager.pauseExecution();
    vertx.eventBus().send(Constants.SOLR_INTERNAL_SEARCH, request);

    return Future.future(promise -> promise.complete());
  }

  public Future<Void> querySolr(String tag, String q, String fl, String start,
                           String rows, String wt, String indent, String sort, String version, int limit) {
    JsonObject qObj = new JsonObject();
    queryParamSetter(qObj, "q", q);
    queryParamSetter(qObj, "fl", fl);
    queryParamSetter(qObj, "start", start);
    queryParamSetter(qObj, "rows", rows);
    queryParamSetter(qObj, "wt", wt);
    queryParamSetter(qObj, "indent", indent);
    queryParamSetter(qObj, "sort", sort);
    queryParamSetter(qObj, "version", version);
    queryParamSetter(qObj, "limit", limit);

    JsonObject request = new JsonObject();
    request.put(com.zee.gwapi.server.Constants.SOLR_QUERY, qObj);
    request.put(com.zee.gwapi.server.Constants.RESP_QUEUE, instanceId.concat("-SOLR_RESP_2"));
    request.put("TAG", tag);

    this.ruleManager.pauseExecution();
    vertx.eventBus().send(Constants.SOLR_INTERNAL_SEARCH, request);

    return Future.future(promise -> promise.complete());
  }

  private void queryParamSetter(JsonObject jo, String pName, Object pValue) {
    if (jo != null && pValue != null)
      jo.put(pName, pValue);
  }

  public boolean respond(String body, int statusCode, String statusMsg) {
    rq = null;

    if(statusMsg == null) {
      context.response()
        .putHeader("content-type", "application/json")
        .end(body);
    } else {
      context.response()
        .putHeader("content-type", "application/json")
        .setStatusCode(statusCode)
        .setStatusMessage(statusMsg)
        .end(body);
    }
    return true;
  }

  public List<String> getList(String... strings) {
    return Arrays.asList(strings);
  }

  public boolean cM(List<String> list) {
    for (String field : list) {
      if (!rq.containsKey(field)) {
        return false;
      }
    }
    return true;
  }

  /* FieldDefaultString */
  public boolean fDs(String field, String defaultValue) {
    if (rq.containsKey(field)) {
      if (rq.getString(field) == null || rq.getString(field).trim().isEmpty())
        rq.put(field, defaultValue);
    } else {
      rq.put(field, defaultValue);
    }

    return true;
  }

  public boolean mdS(String... fields) {
    for (String f : fields) {
      if (!rq.containsKey(f) || rq.getString(f).trim().isEmpty())
        return false;
    }
    return true;
  }

  /* FieldDefaultInteger */
  public boolean fDi(String field, int defaultValue, int lessThan, int greaterThan) {
    int v = Integer.MIN_VALUE;
    if (rq.containsKey(field)) {
      if(rq.getValue(field) instanceof String){
        String val = (String) rq.remove(field);
        rq.put(field, Integer.valueOf(val.trim()));
      }

      if (rq.getInteger(field) == null)
        rq.put(field, defaultValue);
      else {
        if (lessThan != Integer.MIN_VALUE &&
          greaterThan != Integer.MAX_VALUE &&
          rq.getInteger(field) < lessThan && rq.getInteger(field) > greaterThan) {
          rq.put(field, defaultValue);
        } else if (lessThan != Integer.MIN_VALUE &&
          rq.getInteger(field) < lessThan) {
          rq.put(field, defaultValue);
        } else if (greaterThan != Integer.MAX_VALUE &&
          rq.getInteger(field) > greaterThan) {
          rq.put(field, defaultValue);
        }
      }
    } else {
      rq.put(field, defaultValue);
    }
    return true;
  }

  public boolean tokenAuthorisation(String tag) {
    boolean isValid = true;
    try {
      JsonObject jsonObject = TokenValidatorUtil.validateSHA(headers.get("X-ACCESS-TOKEN"), headers.get("Secret-Key"));
      rq.put(tag, jsonObject.getString("product_code") );
    } catch (ParseException e) {
      e.printStackTrace();
      isValid = false;
    } catch (IOException e) {
      e.printStackTrace();
      isValid = false;
    } catch (Exception e){
      e.printStackTrace();
      isValid = false;
    }

    return isValid;
  }

  public Object forEach(List<Object> aList, String expr, String returnType) {

    Map<String, Object> context = new HashMap<>();
    context.put("aList", aList);
    context.put("rq", rq);
    context.put("sUtil", StringUtils.class);
    context.put("cHelper", cHelper);

    Object resp = null;
    try {
      switch (returnType) {
        case "STRING":
          resp = MVEL.evalToString(expr, context);
          break;
        case "BOOL":
          resp = MVEL.evalToBoolean(expr, context);
          break;
        default:
          log.error("Invalid Return Type:{}", returnType);
          break;
      }
    } catch (Exception e) {
      e.printStackTrace();
      resp = null;
    }

    return resp;
  }

  @Override
  public String toString() {
    //log.info(rq.toString());
    return rq.toString();
  }

  public String join(String delemeter, String[] arr) {
    return String.join(delemeter, arr);
  }

  public String join(String delemeter, List<String> list) {
    return String.join(delemeter, list);
  }

  public String trim(String s, String chars, String pos) {
    switch (pos) {
      case "END":
        return StringUtils.stripEnd(s, chars);
      case "START":
        return StringUtils.stripStart(s, chars);
      case "ALL":
        return StringUtils.strip(s, chars);
      default:
        log.info("Invalid trim pos:{}", pos);
    }
    return s;
  }

  public JsonArray getJArray(List<Object> list, String prefix, String sufix) {
    JsonArray arr = new JsonArray();
    for (Object object : list) {
      if (prefix != null) {
        object = prefix + object.toString();
      }

      if (sufix != null) {
        object = object.toString() + sufix;
      }

      arr.add(object.toString());

    }
    return arr;
  }

  public void getJsonObj(String key, String value) {
    try {
      JsonObject jo = new JsonObject(value);
      rq.put(key, jo);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public JsonArray getJArray(Object[] list, String prefix, String sufix) {
    JsonArray arr = new JsonArray();
    for (Object object : list) {

      if (prefix != null) {
        object = prefix + object.toString();
      }

      if (sufix != null) {
        object = object.toString() + sufix;
      }
      arr.add(object.toString());
    }
    return arr;
  }

  public void addJsonObject(String key){
    rq.put(key, new JsonObject());
  }

  public void addJsonArray(String key){
    rq.put(key, new JsonArray());
  }

  public List<Object> arrayToList(Object[] arr){
    return Arrays.asList(arr);
  }

}
