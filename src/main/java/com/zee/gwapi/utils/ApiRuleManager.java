package com.zee.gwapi.utils;

import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.*;

public class ApiRuleManager implements Iterator<JsonObject> {

  private HashMap<String, JsonObject> allRules;
  private ArrayDeque<String> ruleQueue;
  private String currentRuleId;
  private String apiId;
  private boolean doExecute ;

  private ExpressionParser parser = null;
  private OperationContext opContext = null;
  private StandardEvaluationContext spelContext = null;

  private static final Logger log = LoggerFactory.getLogger(ApiRuleManager.class);

  public ApiRuleManager(String apiId, OperationContext opContext){
    this.apiId = apiId;
    setRuleBase();

    this.opContext = opContext;
    this.opContext.setRuleManager(this);
    parser = new SpelExpressionParser();
    spelContext = new StandardEvaluationContext(this.opContext);
    doExecute = true;
  }

  public String getApiId(){
    return apiId;
  }

  public boolean setRuleBase(){

    try {
      log.info("Setting RuleBase for ApiId:{}", apiId);
      setAllRules(ConfigCache.DEFAULT.getRules(apiId));
      setRuleQueue();
    }catch(Exception e){
      log.error("Failed in setting RuleBase for Api : {}", apiId);
      e.printStackTrace();
      return false;
    }

    return true;
  }

  private void setAllRules(HashMap<String, JsonObject> rules){

    if( allRules == null )
      allRules = new HashMap<>();
    else
      allRules.clear();

    allRules.putAll(rules);
  }

  private void setRuleQueue(){
    currentRuleId = null;

    if( ruleQueue == null )
      ruleQueue = new ArrayDeque<>();
    else
      ruleQueue.clear();

    if(allRules==null)
      return;

    ArrayList<String> ruleList = new ArrayList<>();
    log.debug("allRules count:{}", allRules.size());
    for(String key: allRules.keySet()){
      if(key.startsWith("0")){
        ruleList.add(key);
        log.debug("key {} Added", key);
      }
    }

    Comparator<Object> ruleCompairator = (a,b)-> Integer.compare(Integer.parseInt(a.toString().substring(2)), Integer.parseInt(b.toString().substring(2)));

    Arrays.stream(ruleList.toArray()).sorted(ruleCompairator).forEach( rule -> {
      ruleQueue.add((String)rule);
    });
  }

  @Override
  public boolean hasNext() {

    if(opContext.isContextFail()) {
      log.info("[NOTE] ContextFailed");
      return false;
    }

    if(currentRuleId==null && !ruleQueue.isEmpty()) {
      return true;
    }

    if(ruleQueue.isEmpty() || (allRules.get(currentRuleId).getInteger("next_rule")!=null && allRules.get(currentRuleId).getInteger("next_rule")==-1)) {
      return false;
    }

    return true;
  }

  @Override
  public JsonObject next() {
    if(hasNext())
      currentRuleId = ruleQueue.pollFirst();

    if(currentRuleId==null)
      return null;
    log.info("Processing RuleId:{}", currentRuleId);
    return allRules.get(currentRuleId);
  }

  public boolean hasSubrule(boolean resp){
    boolean hasSubrule = false;
    if(!resp || currentRuleId==null)
      return false;

    JsonObject rule = allRules.get(currentRuleId);
    if (rule!=null && rule.getString("result_group") != null && rule.getString("from_rule") != null && rule.getString("to_rule") != null){
      int fromRule = Integer.parseInt(rule.getString("from_rule"));
      int toRule = Integer.parseInt(rule.getString("to_rule"));
      String ruleId = null;

      if(fromRule <= toRule){
        while(fromRule <= toRule) {
          ruleId = rule.getString("result_group").concat("-").concat(Integer.toString(toRule));
          if (allRules.containsKey(ruleId)) {
            ruleQueue.addFirst(ruleId);
          }
          toRule--;
        }
        hasSubrule = true;
      } else {
        log.error("Invalid ResultGroup rule range: FromRule:{}, ToRule:{}", fromRule, toRule);
      }
    }

    return hasSubrule;
  }

  public void execute(){

    Object ruleResponse = null;
    doExecute = true;

    while (hasNext()) {
      JsonObject rule = next();
      try {
        ruleResponse = parser.parseExpression(rule.getString("expression")).getValue(spelContext, Object.class);

        if (ruleResponse instanceof Boolean)
          hasSubrule((Boolean) ruleResponse);

        if(!doExecute)  {
          log.info("Pausing Execution");
          break;
        }

      } catch (Exception e) {
        log.error("Expression: {}", rule.getString("expression"));
        e.printStackTrace();
      }
    }

    if(!hasNext()) {
      clearContext();
    }

  }

  public void clearContext(){
    if(opContext != null)
      opContext.clearContext();

    parser = null;
    spelContext = null;
    opContext = null;
    allRules = null;
    ruleQueue = null;
    currentRuleId = null;
  }

  public void pauseExecution(){
    doExecute = false;
  }

}
