package com.zee.gwapi.utils;

import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

public enum ConfigCache {
  DEFAULT;

  private HashMap<String, JsonObject> apiPathCache;
  private HashMap<String, HashMap<String,JsonObject>> apiRuleCache;
  private List<String> regApis;
  private static final Logger log = LoggerFactory.getLogger(ConfigCache.class);

  public void setRegApis(List<String> apis){
    regApis=apis;
  }

  synchronized public void setApiPathCache(HashMap<String, JsonObject> apiPathCache){
    if(this.apiPathCache == null)
      this.apiPathCache=apiPathCache;
    else
      this.apiPathCache.putAll(apiPathCache);
  }

  synchronized public void setApiRuleCache(HashMap<String, HashMap<String,JsonObject>> apiRuleCache){
    if(this.apiRuleCache == null)
      this.apiRuleCache=apiRuleCache;
    else
      this.apiRuleCache.putAll(apiRuleCache);
    log.info("apiRuleCache : {}", apiRuleCache.size());
  }

  public HashMap<String,JsonObject> getRules(String api){
    log.info("Fetching Rules - api:{}, #rules:{}", api, apiRuleCache.get(api).size());
    return apiRuleCache.get(api);
  }

  public String getApiPath(String api){
    return apiPathCache.get(api).getString(Constants.API_PATH);
  }

  public String getApiMethod(String api){
    return apiPathCache.get(api).getString(Constants.API_METHOD);
  }

  synchronized public void clearAllRuleCache(){
    if(apiRuleCache==null)
      apiRuleCache = new HashMap<>();
    else
      apiRuleCache.clear();
  }

  public boolean updateApiRuleCache(String apiId, HashMap<String,JsonObject> rules){

    if(!regApis.contains(apiId)){
      log.warn("ApiId:{} not registered with the service, Update-Failed", apiId);
      return false;
    }

    if(apiRuleCache!=null && apiRuleCache.containsKey(apiId)) {
      log.info("Updating Rules in Cache for apiId:{}", apiId);
      apiRuleCache.replace(apiId, rules);
      return true;
    }

    return false;
  }

  public boolean clearRuleCache(String apiId){
    if(apiRuleCache==null)
      return false;

    if(apiRuleCache.containsKey(apiId)) {
      apiRuleCache.remove(apiId);
      return true;
    }

    return false;
  }

  public boolean isInitialized(){

    if(regApis==null || regApis.isEmpty() || apiRuleCache==null || apiPathCache==null)
      return false;

    for(String api : regApis){
      if(!apiRuleCache.containsKey(api))
        return false;

      if(!apiPathCache.containsKey(api))
        return false;
    }

    return true;
  }

}
