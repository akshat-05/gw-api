package com.zee.gwapi.utils;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.text.StringEscapeUtils;


import java.io.UnsupportedEncodingException;
import java.util.Date;

import java.text.SimpleDateFormat;

public class CommonHelper {
    public boolean isSet(Object obj){
        return obj != null;
    }

    public String formatDate(String date){
        try{
            return new SimpleDateFormat("yyyy-MM-dd").format(new Date(Long.parseLong(date)));
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String prefixTranslation(String translation){
        return "lang-" +translation;
    }

    // public JsonObject createJsonObject(String key){
    //     return new JsonObject().put(key, new JsonObject());
    // }

    public String site_imageurl(){
        return "https://akamaividz.zee5.com/resources/";
    }

    public String site_cover_imageurl(){
        return "https://akamaividz1.zee5.com/resources/";
    }

    public String image_fullpath(String id, String list, String asset_type){
        try{
            String image_extension = ".jpg";
            String image_default_dimension = "270x152";
            if(asset_type.equals("9")){
                image_extension = ".png";
                image_default_dimension = "170x170";
            }

            String image_url = site_imageurl()+id+"/list/"+image_default_dimension+"/"+list+image_extension;
            return image_url;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String cover_fullpath(String id, String list, String asset_type){
        try{
            String image_extension = ".jpg";
            String image_default_dimension = "270x405";
            String cover_image_domain = site_cover_imageurl();
            if(asset_type.equals("9")){
                image_extension = ".png";
                image_default_dimension = "170x120";
                cover_image_domain = site_imageurl();
            }

            String image_url = cover_image_domain+"/cover/"+image_default_dimension+"/"+list+image_extension;
            return image_url;
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public static String replceSpecialCharacter(String inputString) throws UnsupportedEncodingException {
      inputString = inputString.replace("&", "and");
      return returnPostSlug(inputString);
    }

    public static String returnPostSlug(String inputString) throws UnsupportedEncodingException {
      //convert input string into UTF-32
      byte[] utf32 = inputString.getBytes("UTF-32");
      String utf32Str = StringUtils.newString(utf32, "UTF-32");

      //convert, converted UTF-32 string back to UTF-8
      byte[] utf8 = utf32Str.getBytes("UTF-8");
      String utf8Str = StringUtils.newString(utf8, "UTF-8");

      if(!inputString.equals(utf8Str)){
        //TODO: Detect encoding used
        byte[] inputStringBytes = inputString.getBytes("UTF-8");
        inputString = StringUtils.newString(inputStringBytes, "UTF-8");
      }

      inputString = StringEscapeUtils.escapeHtml4(inputString);
      inputString = inputString.replaceAll("&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);", "\\1");
      inputString = StringEscapeUtils.unescapeHtml4(inputString);
      inputString = inputString.replaceAll("[^a-zA-Z0-9]", "-");
      inputString = inputString.replaceAll("[-]+", "-");
      inputString = org.apache.commons.lang3.StringUtils.strip(inputString); // trim whitespaces from beginning and end
      inputString = org.apache.commons.lang3.StringUtils.strip(inputString, "-"); // trim "-" from beginning and end
      inputString = inputString.toLowerCase();
      return inputString;
    }

}
