package com.zee.gwapi.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.vertx.core.json.JsonObject;
import org.jose4j.json.internal.json_simple.parser.ParseException;

import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.Key;
import java.util.Base64;

public class TokenValidatorUtil {

  public static JsonObject validateSHA(String token, String secret) throws IOException, ParseException {
    Key hmacKey = new SecretKeySpec(Base64.getDecoder().decode(secret),
      SignatureAlgorithm.HS256.getJcaName());

    //To generate token for testing purpose
  /*  Instant now = Instant.now();
    String jwtToken = Jwts.builder()
      .claim("name", "Jane Doe")
      .claim("email", "jane@example.com")
      .setSubject("jane")
      .setId(UUID.randomUUID().toString())
      .setIssuedAt(Date.from(now))
      .signWith(hmacKey)
      .compact();

    System.out.println("Hello token " + jwtToken);*/

    Jws<Claims> jwt = Jwts.parserBuilder()
      .setSigningKey(hmacKey)
      .build()
      .parseClaimsJws(token);
    System.out.println("Hello claims " + jwt.getBody());
    return mapJsonToJWTClaimEnity(jwt.getBody());
  }

  //Convert claims to Entity
  private static JsonObject mapJsonToJWTClaimEnity(Claims claim) throws IOException, ParseException {
    JsonObject decodedJsonObject = new JsonObject(claim);
    return decodedJsonObject;
  }
}
