package com.zee.gwapi.utils;

public class Constants {

  public static final String AS_PAYLOAD = "autoSuggest";
  public static final String DB_CONFIG = "db";
  public static final String REG_APIs = "api";
  public static final String SOLR_CONFIG = "solrConfig";

  public static final String SERVER_CONFIG = "server";
  public static final String SERVER_PORT = "port";
  public static final String API_PATH = "api_path";
  public static final String API_METHOD = "http_method";

  public static final String RESP_TIMEOUT = "respTimeout";
  public static final String TIMEOUT_CODE = "timeoutCode";
  public static final String TIMEOUT_MSG = "timeoutMsg";

}
